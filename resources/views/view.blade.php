@extends('layout')
@section('title') View - CRUD @stop
@section('body')
<body id="signup">
	<div class="container">
		<div class="row header">
			<div class="col-md-12">
				<h3 class="logo">
					<a href="index.html">Hola!</a>
				</h3>
				<h4>Here are the details you asked for:</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="wrapper clearfix">
					<div class="formy">
						<div class="row">

							<div class="col-md-12">
								{{ Form::model($user) }}
								<fieldset disabled="">
									<div class="form-group">
							    		<label for="name">Your firstname</label>
							    		{{ Form::text('firstname',null,['class'=>'form-control']) }}
							  		</div>
							  		<div class="form-group">
							    		<label for="name">Your lastname</label>
							    		{{ Form::text('lastname',null,['class'=>'form-control']) }}
							  		</div>
							  		<div class="form-group">
							    		<label for="name">Your Email</label>
							    		{{ Form::text('email',null,['class'=>'form-control']) }}
							  		</div>
								</fieldset>
								<div class="submit">
									<a href="/" class="button-clear">
										<span>View another record.</span>
  									</a>
								</div>
								{{ Form::close() }}
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
<div id="footer">
	
	<div class="container">
		<div class="row">
			<div class="col-sm-3 copyright">
				Copyrights 2017
			</div>
			<div class="col-sm-6 menu">
				<ul>
	  				<li>
        				<a href="features.html">Features</a>
				  	</li>
				    <li>
				       	<a href="services.html">Services</a>
				    </li>
				    <li>
				    	<a href="pricing.html">Pricing</a>
				    </li>
				    <li>
				        <a href="support.html">Support</a>
				    </li>
				    <li>
				    	<a href="blog.html">Blog</a>
				    </li>
				</ul>
			</div>
			<div class="col-sm-3 social">
				<a href="#">
					<img src="/images/social/social-tw.png" alt="twitter" />
				</a>
				<a href="#">
					<img src="/images/social/social-dbl.png" alt="dribbble" />
				</a>					
			</div>
		</div>
	</div>
</div>
</body>
@stop