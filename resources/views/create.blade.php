@extends('layout')
@section('title') Create - CRUD @stop
@section('body')
<body id="signup">
	<div class="container">
		<div class="row header">
			<div class="col-md-12">
				<h3 class="logo">
					<a href="index.html">Hola!</a>
				</h3>
				<h4>Enter user details.</h4>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="wrapper clearfix">
					<div class="formy">
						<div class="row">
							<div class="col-md-12">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
								</ul>
								</div>
							@endif
								{!! Form::open(['url' => '/user']) !!}
									<div class="form-group">
							    		<label for="name">Your firstname</label>
							    		{!! Form::text('firstname','',['class'=>'form-control']) !!}
							  		</div>
							  		<div class="form-group">
							    		<label for="name">Your lastname</label>
							    		{!! Form::text('lastname','',['class'=>'form-control']) !!}
							  		</div>
							  		<div class="form-group">
							    		<label for="name">Your Email</label>
							    		{!! Form::text('email','',['class'=>'form-control']) !!}
							  		</div>
							  		<div class="submit">
							  			<button type="submit" class="btn btn-info">Create</button>
							  		</div>
								{!! Form::close() !!}
								<!-- <form role="form" method="POST" action="create_post.php">
									<div class="form-group">
							    		<label for="name">Your name</label>
							    		<input type="text" class="form-control" id="name" name="name" />
							  		</div>
							  		<div class="form-group">
							    		<label for="fathername">Father's name</label>
							    		<input type="text" class="form-control" id="fathername" name = "fathername" />
							  		</div>
							  		<div class="form-group">
							    		<label for="email">Email address</label>
							    		<input type="email" class="form-control" id="email" name = "email" />
							  		</div>
							  		<div class="form-group">
							    		<label for="address">Permanent Address</label>
							    		<input type="text" class="form-control" id="address" name = "address" />
							  		</div>
							  		<div class="form-group">
							    		<label for="state">State</label>
							    		<input type="text" class="form-control" id="state" name = "state" />
							  		</div>
							  		<div class="form-group">
							    		<label for="pin">Pin code</label>
							    		<input type="number" class="form-control" id="pin" name = "pin" />
							  		</div>
							  		<div class="form-group">
							    		<label for="contact">Contact no:</label>
							    		<input type="text" class="form-control" id="contact" name = "contact" />
							  		</div>
							  		<div class="submit">
							  			<button type="submit" class="btn btn-info">Create</button>
							  		</div>
								</form> -->
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
<div id="footer">
	
	<div class="container">
		<div class="row">
			<div class="col-sm-3 copyright">
				Copyrights 2017
			</div>
			<div class="col-sm-6 menu">
				<ul>
	  				<li>
        				<a href="features.html">Features</a>
				  	</li>
				    <li>
				       	<a href="services.html">Services</a>
				    </li>
				    <li>
				    	<a href="pricing.html">Pricing</a>
				    </li>
				    <li>
				        <a href="support.html">Support</a>
				    </li>
				    <li>
				    	<a href="blog.html">Blog</a>
				    </li>
				</ul>
			</div>
			<div class="col-sm-3 social">
				<a href="#">
					<img src="/images/social/social-tw.png" alt="twitter" />
				</a>
				<a href="#">
					<img src="/images/social/social-dbl.png" alt="dribbble" />
				</a>					
			</div>
		</div>
	</div>
</div>
</body>
@stop