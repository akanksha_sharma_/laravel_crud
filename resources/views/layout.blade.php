<!DOCTYPE html>
<html>
<head>
    @include('partials.meta')
    @include('partials.styles')
    @include('partials.scripts')
</head>
@yield('body')
</html>