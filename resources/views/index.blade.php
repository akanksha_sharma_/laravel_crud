@extends('layout') 
@section('title') Home - CRUD @stop 
@section('body')
<body id="home3">
    <div class="st-container">
        <div class="st-pusher">
            <div class="st-content">
                <div id="hero">
                    <div id="cover-image" class="animated fadeIn">
                        <div class="container">
                            <h1 class="hero-text animated fadeIn">USER MAINTENANCE SYSTEM</h1>
                            <div class="cta animated fadeIn">
                                <a href="user/create" class="button">Create A Record</a>
                                <a href="#records" class="demo">Show all records.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container" id="records">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>View</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->firstname}}</td>
                            <td>{{$user->lastname}}</td>
                            <td><a class='btn btn-info btn-small' href='user/{{$user->id}}'>View</a></td>
                            <td><a class='btn btn-success btn-small' href='user/{{$user->id}}/edit'>Edit</a></td>
                            {!! Form::open(['url' => "/user/$user->id" , 'method' => 'DELETE']) !!}
                            <td>
                            <button type="submit" class="btn btn-danger btn-small">Delete</button>
                            </td>
                            {!! Form::close() !!}
                            
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
                <div id="footer">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3 copyright">
                                Copyrights 2017
                            </div>
                            <div class="col-sm-6 menu">
                                <ul>
                                    <li>
                                        <a href="features.html">Features</a>
                                    </li>
                                    <li>
                                        <a href="services.html">Services</a>
                                    </li>
                                    <li>
                                        <a href="pricing.html">Pricing</a>
                                    </li>
                                    <li>
                                        <a href="support.html">Support</a>
                                    </li>
                                    <li>
                                        <a href="blog.html">Blog</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-3 social">
                                <a href="#">
                                    <img src="images/social/social-tw.png" alt="twitter" />
                                </a>
                                <a href="#">
                                    <img src="images/social/social-dbl.png" alt="dribbble" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@if(Session::has('success'))
<script type="text/javascript">
    swal('Success',
        'Created Succesfully',
        'success');
</script>
@endif
</body>
@stop