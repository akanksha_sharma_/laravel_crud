<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class PageController extends Controller
{
    public function home(){
    	$users = User::paginate(3);
        return view('index')->with('users',$users);
    }
}