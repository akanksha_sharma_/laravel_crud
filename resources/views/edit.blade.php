@extends('layout')
@section('title') EDIT - CRUD @stop
@section('body')
<body id="signup">
	<div class="container">
		<div class="row header">
			<div class="col-md-12">
				<h3 class="logo">
					<a href="index.html">Hola!</a>
				</h3>
				<h4>Update user deatils.</h4>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="wrapper clearfix">
					<div class="formy">
						<div class="row">
							<div class="col-md-12">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
								</ul>
								</div>
							@endif
								{!! Form::model($user,['url' => "/user/$user->id" , 'method' => 'PUT']) !!}
									<div class="form-group">
							    		<label for="name">Your firstname</label>
							    		{!! Form::text('firstname',null,['class'=>'form-control']) !!}
							  		</div>
							  		<div class="form-group">
							    		<label for="name">Your lastname</label>
							    		{!! Form::text('lastname',null,['class'=>'form-control']) !!}
							  		</div>
							  		<div class="form-group">
							    		<label for="name">Your Email</label>
							    		{!! Form::text('email',null,['class'=>'form-control']) !!}
							  		</div>
									<div class="submit">
							  			<button type="submit" class="btn btn-info">Update</button>
							  		</div>
								{!! Form::close() !!}
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
<div id="footer">
	
	<div class="container">
		<div class="row">
			<div class="col-sm-3 copyright">
				Copyrights 2017
			</div>
			<div class="col-sm-6 menu">
				<ul>
	  				<li>
        				<a href="features.html">Features</a>
				  	</li>
				    <li>
				       	<a href="services.html">Services</a>
				    </li>
				    <li>
				    	<a href="pricing.html">Pricing</a>
				    </li>
				    <li>
				        <a href="support.html">Support</a>
				    </li>
				    <li>
				    	<a href="blog.html">Blog</a>
				    </li>
				</ul>
			</div>
			<div class="col-sm-3 social">
				<a href="#">
					<img src="/images/social/social-tw.png" alt="twitter" />
				</a>
				<a href="#">
					<img src="/images/social/social-dbl.png" alt="dribbble" />
				</a>					
			</div>
		</div>
	</div>
</div>
</body>
@stop