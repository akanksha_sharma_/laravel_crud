<!-- stylesheets -->
<link rel="stylesheet" type="text/css" href="/css/compiled/theme.css">
<link rel="stylesheet" type="text/css" href="/css/vendor/font-awesome.css">
<link rel="stylesheet" type="text/css" href="/css/vendor/animate.css">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/6.4.2/sweetalert2.min.css">
 <link rel="stylesheet" type="text/css" href="/style.css">
