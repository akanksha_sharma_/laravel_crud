<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class User extends Model
{
    protected $fillable = ['firstname','lastname','email'];

    public function rules(){
		return $rules=[
		'firstname' => 'required|min:3',
		'lastname' => 'required|min:3',
		'email' => 'required|email'
		];    	
    }

    public function storeData($data){
   
    $validator = Validator::make($data,$this->rules());
    if($validator->passes()){
    	User::create($data);
    }
    else{
    	return $validator->messages();
    }
    }

    public function updateData($user,$data){
    	$validator = Validator::make($data,$this->rules());
    	if($validator->passes()){
    		$user->fill($data)->save();
    	}
    	else{
    		return $validator->messages();
    	}
    }
}
